



//callback

//             const temperatureSpan = document.getElementById('temparature');

// // Define the callback function for handling the fetch response
// function handleResponse(response) {
//     // Check if the response status is OK
//     if (response.ok) {
//         // Parse the JSON data from the response
//         response.json(function(data) {
//             // Extract the temperature from the parsed data
//             const temperature = data.current.temperature_2m;
//             // Set the temperature value to the temperatureSpan element
//             temperatureSpan.innerHTML = temperature;
//         });
//     } else {
//         // Log an error message if the response is not OK
//         console.error('Response not OK:', response.statusText);
//     }
// }

// // Perform the fetch request and pass the handleResponse callback function
// fetch('https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m')
//     .then(handleResponse)
//     .catch(error => console.log(error));







// fetch('https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m')



button.addEventListener('click', async (event) => {

   event.preventDefault()
   

   const latitudeInput = document.getElementById('latitude');
   const latitudeNumber = parseFloat(latitudeInput.value);

   const longitudeInput = document.getElementById('longitude');
   const longitudeNumber = parseFloat(longitudeInput.value);

   const resultSpan = document.getElementById('result');
   const para = document.getElementById('para')


   if (
      isNaN(latitudeNumber) || latitudeNumber < -90 || latitudeNumber > 90 ||
      isNaN(longitudeNumber) || longitudeNumber < -180 || longitudeNumber > 180
    ) {
     
    para.innerHTML = "Please Enter Valid  Latitude AndLongitude ";
    resultSpan.innerHTML = ""; 
    return; 
    }
    else{
      para.innerHTML = '';
    }


   try {

      const response = await fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitudeNumber}&longitude=${longitudeNumber}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)

      if (!response.ok) {
         throw new Error('Failed to fetch data')
      }

      const result = await response.json()
      resultSpan.innerHTML = "Temaparature is " + result.current.temperature_2m + "&deg;C"

   }
   catch (error) {
      console.error('Error:' + error)
      resultSpan.innerHTML = "An error occured .Please try again later"
   }



})













